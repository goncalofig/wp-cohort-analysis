<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('COHO_USER' ) ) :

class COHO_USER {

    /**
     * The single instance of the class.
     *
     * @var     COHO_USER
     */
    private static $instance = null;


    function get_user_step_id_in_flow( $flow_id, $date = '' ) {

        $date = empty($date) ? date( 'Y-m-d') : $date;

        $user_id = get_current_user_id();

        global $wpdb;

        return $wpdb->get_var( $wpdb->prepare(
            "SELECT
                        MAX(wp_coho_user_interactions.step_id)
                    FROM
                        `wp_coho_user_interactions`
                    INNER JOIN `wp_coho_steps` ON wp_coho_steps.step_id = wp_coho_user_interactions.step_id
                    INNER JOIN `wp_coho_flows` ON wp_coho_flows.flow_id = wp_coho_steps.flow_id
                    WHERE
	                    coho_user_id = %d
                        AND wp_coho_flows.flow_id = %d
                        AND wp_coho_user_interactions.time = %s
                        ",
            $user_id,
            $flow_id,
            $date
        ) );
    }


    function get_user_cohort_in_test( $test_id ) {

        $user_id = get_current_user_id();

        $cohort_id = wp_cache_get( $user_id, 'coho_test_' . $test_id . '_user_cohort' );

        if ( !$cohort_id ) {

            global $wpdb;

            $cohort_id = $wpdb->get_var( $wpdb->prepare(
                "SELECT
                        wp_coho_cohort_users.cohort_id
                    FROM
                        `wp_coho_cohort_users`
                    INNER JOIN `wp_coho_cohorts` ON wp_coho_cohort_users.cohort_id = wp_coho_cohorts.cohort_id
                    WHERE
                        wp_coho_cohort_users.coho_user_id = %d 
                        AND wp_coho_cohorts.test_id = %d
                        ",
                $user_id,
                $test_id
            ) );

            wp_cache_set( $user_id, $cohort_id, 'coho_test_' . $test_id . '_user_cohort', 1800 );
        }

        return (int) $cohort_id;
    }


    /**
     * Check if user is in given cohort.
     *
     * @param   int     $test_id
     * @param   int     $cohort_id
     * @return  bool
     */
    function user_is_in_cohort( $test_id, $cohort_id ) {

        $user_test_cohort_id = $this->get_user_cohort_in_test( $test_id );

        return $cohort_id === $user_test_cohort_id;
    }


    /**
     * COHORT_ANALYSIS_MAIN Instance.
     *
     * Ensures only one instance of COHORT_ANALYSIS_MAIN is loaded or can be loaded.
     *
     * @static
     *
     * @return COHO_USER - Main instance.
     */
    public static function instance() {

        if ( is_null( self::$instance ) ) {

            self::$instance = new self;
        }
        return self::$instance;
    }
}


/**
 * Cohort User instance.
 *
 * Returns the main instance to prevent the need to use globals.
 *
 * @return COHO_USER - Main instance
 */
function coho_user() {
    return COHO_USER::instance();
}

// initialize
coho_user();

endif;