<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('COHO_Data' ) ) :

/**
 * Abstract COHO Data Class
 *
 * Implemented by classes using the same CRUD(s) pattern.
 */
abstract class COHO_Data {

    /**
     * ID for this object.
     *
     * @var int
     */
    protected $id;


    /**
     * Core data for this object. Name value pairs (name + default value).
     *
     * @var array
     */
    protected $data = array();


    /**
     * Returns the unique ID for this object.
     *
     * @return int
     */
    public function get_id() {
        return $this->id;
    }


    /**
     * Returns the unique slug for this object.
     *
     * @return int
     */
    public function get_slug() {
        return $this->get_prop('slug');
    }


    /**
     * Gets a prop for a getter method.
     *
     * @param   string  $prop   Name of prop to get.
     * @return  mixed
     */
    protected function get_prop( $prop ) {
        $value = null;

        if ( array_key_exists( $prop, $this->data ) ) {
            $value = $this->data[ $prop ];
        }

        return $value;
    }


    /**
     * Set ID.
     *
     * @param int $id ID.
     */
    protected function set_id( $id ) {
        $this->id = absint( $id );
    }


    /**
     * Sets a prop for a setter method.
     *
     * @param   string  $prop   Name of prop to set.
     * @param   mixed   $value  Value of the prop.
     */
    protected function set_prop( $prop, $value ) {
        if ( array_key_exists( $prop, $this->data ) ) {
            $this->data[ $prop ] = $value;
        }
    }
}

endif;