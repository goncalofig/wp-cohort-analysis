<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('COHO_INSTALLER' ) ) :

class COHO_INSTALLER {

    private $tables = array(
        'coho_flows',
        'coho_steps',
        'coho_user_interactions',
        'coho_step_totals',
        'coho_tests',
        'coho_cohorts',
        'coho_cohort_users'
    );


    private $drop_tables_on_deactivation = true;


    function get_table_names() {

        return $this->tables;
    }


    function activate_plugin() {

        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $tables = $this->get_table_names();

        foreach( $tables as $table_name ) {

            $function_name = 'get_' . $table_name . '_table_sql';

            $table_full_name = $wpdb->prefix . $table_name;

            $sql = $this->$function_name( $table_full_name, $charset_collate );

            dbDelta($sql);
        }

        add_option( 'coho_db_version', coho()->get_db_version() );
    }


    function get_coho_flows_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                flow_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                name varchar(100) NOT NULL,
                description varchar(200),
                active bool NOT NULL,
                PRIMARY KEY  (flow_id)
                ) $charset_collate;";
    }

    function get_coho_steps_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                step_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                flow_id bigint(20) unsigned NOT NULL,
                num tinyint(1) unsigned NOT NULL,
                name varchar(100) NOT NULL, 
                description varchar(200),
                hook varchar(100),
                PRIMARY KEY  (step_id)
                ) $charset_collate;";
    }

    function get_coho_user_interactions_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                inter_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                time date DEFAULT '0000-00-00' NOT NULL,
                coho_user_id bigint(20) unsigned NOT NULL,
                step_id smallint unsigned NOT NULL,
                PRIMARY KEY  (inter_id)
                ) $charset_collate;";
    }

    function get_coho_step_totals_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                total_id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                date date DEFAULT '0000-00-00' NOT NULL,
                step_id smallint unsigned NOT NULL,
                value int unsigned NOT NULL,
                PRIMARY KEY  (total_id)
                ) $charset_collate;";
    }

    function get_coho_tests_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                test_id bigint(20) unsigned NOT NULL,
                name varchar(100) NOT NULL,
                slug varchar(100) NOT NULL,
                description varchar(200),
                start_date  date DEFAULT '0000-00-00' NOT NULL,
                end_date  date DEFAULT '0000-00-00' NOT NULL,
                PRIMARY KEY  (test_id)
                ) $charset_collate;";
    }

    function get_coho_cohorts_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                cohort_id bigint(20) unsigned NOT NULL,
                test_id bigint(20) unsigned NOT NULL,
                name varchar(100) NOT NULL,
                slug varchar(100) NOT NULL,
                description varchar(200),
                PRIMARY KEY  (cohort_id)
                ) $charset_collate;";
    }

    function get_coho_cohort_users_table_sql( $table_full_name, $charset_collate ) {

        return "CREATE TABLE $table_full_name (
                cohort_id bigint(20) unsigned NOT NULL,
                coho_user_id bigint(20) unsigned NOT NULL,
                PRIMARY KEY  (cohort_id, coho_user_id)
                ) $charset_collate;";
    }


    function deactivate_plugin() {

        $drop_tables = $this->drop_tables_on_deactivation;

        if( !$drop_tables ) return;

        global $wpdb;

        $tables = $this->get_table_names();

        foreach( $tables as $table_name ) {

            $table_full_name = $wpdb->prefix . $table_name;

            $sql = "DROP TABLE IF EXISTS $table_full_name";

            $wpdb->query($sql);
        }

        delete_option('coho_db_version' );
    }
}

endif;