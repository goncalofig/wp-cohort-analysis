<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('COHO_Feature_Test' ) ) :

class COHO_Feature_Test extends COHO_Data {


    /**
     * Stores test data.
     *
     * @var array
     */
    protected $data = array(
        'name'          => '',
        'slug'          => '',
        'description'   => '',
        'start_date'    => '0000-00-00',
        'end_date'      => '0000-00-00',
        'cohorts'       => array()              // Array with cohort slug as keys and COHO_Cohort instances as values.
    );


    /**
     * Constructor.
     *
     * @param COHO_Feature_Test|object  $test   Test object.
     */
    function __construct( $test ) {

        $this->set_id($test->test_id);

        unset($test->test_id);

        foreach ( get_object_vars( $test ) as $key => $value ) {
            $this->set_prop($key, $value);
        }

        $this->read_cohorts();
    }


    /**
     * Get test cohort by slug.
     *
     * @param   string              $slug
     * @return  COHO_Cohort|null
     */
    function get_cohort( $slug ) {

        $cohorts = $this->get_prop('cohorts');

        if ( !array_key_exists( $slug, $cohorts ) ) return null;

        return $cohorts[$slug];
    }


    /**
     * Constructor.
     *
     * @return  bool    $is_active      Whether the test is active or not.
     */
    function is_active() {

        $now = time();

        $start_date = strtotime($this->get_prop('start_date'));

        $end_date = strtotime($this->get_prop('end_date'));

        return ($now >= $start_date) && ($now <= $end_date);
    }


    /**
     * Get test cohorts.
     */
    function read_cohorts() {

        global $wpdb;

        $_cohorts = wp_cache_get( $this->get_prop('slug'), 'coho_test_cohorts' );

        if( !$_cohorts ) {

            $_cohorts = $wpdb->get_results( $wpdb->prepare(
                "SELECT * FROM `wp_coho_cohorts` WHERE test_id = %d",
                $this->get_id()
            ) );

            wp_cache_add( $this->get_prop('slug'), $_cohorts, 'coho_test_cohorts' );
        }

        if( !$_cohorts ) return;

        $cohorts = array();

        foreach( $_cohorts as $_cohort ) {

            $cohort = new COHO_Cohort( $_cohort );

            $cohorts[$cohort->get_prop('slug')] = $cohort;
        }

        $this->set_prop('cohorts', $cohorts );
    }


    /**
     * Retrieve COHO_Feature_Test instance.
     *
     * @global  wpdb    $wpdb   WordPress database abstraction object.
     *
     * @param   string                      $test_slug
     * @return  COHO_Feature_Test|false     $test           Test object, false otherwise.
     */
    public static function get_instance( $test_slug ) {

        global $wpdb;

        if ( empty($test_slug) ) return false;

        wp_cache_delete( $test_slug, 'coho_tests' );

        $_test = wp_cache_get( $test_slug, 'coho_tests' );

        if ( !$_test ) {

            $_test = $wpdb->get_row( $wpdb->prepare(
                "SELECT * FROM `wp_coho_tests` WHERE slug = %s LIMIT 1",
                $test_slug
            ) );

            if ( !$_test ) return false;

            wp_cache_add( $_test->slug, $_test, 'coho_tests' );
        }

        return new COHO_Feature_Test( $_test );
    }
}

endif;