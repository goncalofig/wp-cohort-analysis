<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('CH_Flow_Shop_Order' ) ) :

class CH_Flow_Shop_Order {

    public $flow;

    public $steps;


    function __construct() {

        // Hooking functionality to init since at this time the user is already authenticated
        add_action( 'init', array( $this, 'init_flow' ) );

        // Cron job needs to be hooked before init
        add_action( 'coho_daily_record_registered_customers_count', array( $this, 'record_registered_customers_count' ) );
    }


    /**
     *
     *
     */
    function init_flow() {

        // This flow applies to customer users only
        $user = wp_get_current_user();
        if( ! in_array('customer', $user->roles ) ) return;


        $flow = array(
            'id'                => 1,
            'name'              => __( "Shop Order", 'coho' ),
            'description'       => __( "User flow to register a order", 'coho' )
        );


        $this->set_flow($flow);


        $steps = array(
            array(
                'id'            => 1,
                'num'           => 0,
                'name'          => __( "Registered but didn't login", 'coho' ),
                'description'   => __( "Registered users that didn't visit the website", 'coho' )
            ),
            array(
                'id'            => 2,
                'num'           => 1,
                'name'          => __( "Logged in", 'coho' ),
                'description'   => __( "Registered users that visited the website, while logged in", 'coho' ),
                'hook'          => 'get_header'
            ),
            array(
                'id'            => 3,
                'num'           => 2,
                'name'          => __( "Accessed product archive", 'coho' ),
                'description'   => "",
                'hook'          => 'woocommerce_archive_description'
            ),
            array(
                'id'            => 4,
                'num'           => 3,
                'name'          => __( "Accessed single product", 'coho' ),
                'description'   => "",
                'hook'          => 'woocommerce_before_single_product_summary'
            ),
            array(
                'id'            => 5,
                'num'           => 4,
                'name'          => __( "Ordered", 'coho' ),
                'description'   => "",
                'hook'          => 'woocommerce_thankyou'
            )
        );

        $this->set_steps($steps);

        add_action( 'shutdown', array( $this, 'record_user_interaction' ) );
    }


    function set_flow($flow) {

        $this->flow = $flow;
    }


    function get_flow() {

        return $this->flow;
    }


    function get_flow_id() {

        return $this->get_flow()['id'];
    }


    function set_steps($steps) {

        $this->steps = $steps;
    }


    function get_steps() {

        return $this->steps;
    }


    function create_flow( $flow ) {

        global $wpdb;

        $table_full_name = $wpdb->prefix . 'coho_flows';

        $data = array(
            'name'          => $flow['name'],
            'description'   => $flow['description'],
            'active'        => 0
        );

        $wpdb->insert( $table_full_name, $data, array( '%s', '%s', '%d') );
    }


    function activate_flow() {

        $tomorrow = strtotime('tomorrow' );

        if( !wp_next_scheduled( 'coho_daily_record_registered_customers_count' ) ) {
            wp_schedule_event( $tomorrow, 'daily', 'coho_daily_record_registered_customers_count' );
        }
    }


    function deactivate_flow() {

        wp_clear_scheduled_hook( 'coho_daily_record_registered_customers_count' );
    }


    function record_registered_customers_count() {

        $customer_count = count_users()['avail_roles']['customer'];

        global $wpdb;

        $table_full_name = $wpdb->prefix . 'coho_step_totals';

        $data = array(
            'date'      => date( 'Y-m-d'),
            'step_id'   => 1,
            'value'     => $customer_count
        );

        $wpdb->insert( $table_full_name, $data, array( '%s', '%d', '%d') );
    }


    function record_user_interaction() {

        /*
         * Step 1: Get step
         */

        // Get all actions called
        global $wp_actions;
        $wp_actions = array_keys($wp_actions);  // Only get action names


        // Get steps associated with called hooks
        $steps = $this->get_steps();

        $current_steps = array_filter($steps, function($step) use ($wp_actions) {

            if( isset($step['hook']) ) {
                return in_array($step['hook'], $wp_actions);
            } else {
                return false;
            }
        });

        if( empty($current_steps) ) return;


        // Get the step highest in the order.
        if( sizeof($current_steps) > 1 ) {

            usort($current_steps, function($a, $b) {

                return $a['num'] < $b['num'] ? -1 : 1;
            });

            $current_step = array_pop($current_steps);

        } else {

            // $current_steps is an associative array with (a single) $key => $value pair, where $key
            // is kept from the original $steps array (array_filter keeps keys) and the $step as value.
            $current_step = array_values($current_steps)[0];
        }


        /*
         * Step 2: Check if record already exists
         */
        $flow_id = $this->get_flow_id();

        $user_step_id = coho_user()->get_user_step_id_in_flow($flow_id);

        if( $user_step_id >= $current_step['id']  ) return;


        /*
         * Step 3: If record doesn't exist, insert it
         */

        global $wpdb;

        $table_full_name = $wpdb->prefix . 'coho_user_interactions';

        $data = array(
            'time'          => date( 'Y-m-d'),
            'coho_user_id'  => get_current_user_id(),
            'step_id'       => $current_step['id']
        );

        if( $user_step_id ) {

            $where = array(
                'time'          => date( 'Y-m-d'),
                'coho_user_id'  => get_current_user_id(),
                'step_id'       => $user_step_id
            );

            $wpdb->update( $table_full_name, $data, $where, array( '%s', '%d', '%d') );

        } else {

            $wpdb->insert( $table_full_name, $data, array( '%s', '%d', '%d') );
        }
    }
}

endif;