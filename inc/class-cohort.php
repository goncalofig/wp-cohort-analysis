<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('COHO_Cohort' ) ) :

class COHO_Cohort extends COHO_Data {


    /**
     * Stores cohort data.
     *
     * @var array
     */
    protected $data = array(
        'name'          => '',
        'slug'          => '',
        'description'   => ''
    );


    /**
     * Constructor.
     *
     * @param COHO_Cohort|object  $cohort   Cohort object.
     */
    function __construct( $cohort ) {

        $this->set_id($cohort->cohort_id);

        unset($cohort->cohort_id);

        foreach ( get_object_vars( $cohort ) as $key => $value ) {
            $this->set_prop($key, $value);
        }
    }
}

endif;