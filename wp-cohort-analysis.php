<?php

/*
Plugin Name: WP Cohort Analysis
Description: Cohort Analysis for developers
Version: 1.0
Author: Gonçalo Vieira Figueiredo
Author URI: goncalovf.com
License: GPL2
Text Domain: coho
Domain path: /lang
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('COHORT_ANALYSIS_MAIN' ) ) :

class COHORT_ANALYSIS_MAIN {

    /**
     * The single instance of the class.
     *
     * @var     COHORT_ANALYSIS_MAIN
     */
    private static $instance = null;


    /**
     * The current plugin database version.
     * To be checked with the database version of the installed instance.
     *
     * @var     string
     */
    public $db_version = '1.0';


    /**
     * Whether we're debugging the plugin
     *
     * @var     string
     */
    public $debugging = false;


    /**
     * Active tests that have been requested by code execution
     *
     * @var array
     */
    public $tests = array();



    /**
     * Initiate plugin.
     */
    private function __construct() {

        /*
         * Register activation hook
         */
        $this->safely_require_once( 'inc/plugin-installer.php' );
        $plugin_installer = new COHO_INSTALLER;
        register_activation_hook( __FILE__, array( $plugin_installer, 'activate_plugin' ) );
        register_deactivation_hook( __FILE__, array( $plugin_installer, 'deactivate_plugin' ) );


        if( $this->debugging() ) {

            $this->safely_require_once( 'inc/plugin-debug.php' );
        }


        $this->safely_require_once( 'inc/flows/class-shop-order.php' );
        new CH_Flow_Shop_Order;

        $this->safely_require_once( 'inc/abstract-coho-data.php' );
        $this->safely_require_once( 'inc/class-cohort.php' );
        $this->safely_require_once( 'inc/class-feature-test.php' );

        $this->safely_require_once( 'inc/class-cohort-user.php' );
    }


    /**
     * Get current plugin database version.
     *
     * @return string
     */
    function get_db_version() {

        return $this->db_version;
    }


    function debugging() {

        return $this->debugging;
    }


    function get_tests() {

        return $this->tests;
    }


    function add_test( $test ) {

        $this->tests[] = $test;
    }


    function get_test_from_memory( $test_slug ) {

        foreach ( $this->get_tests() as $test ) {
            if ( $test->get_slug() === $test_slug ) {
                return $test;
            }
        }

        return null;
    }


    function test_instantiated( $test_slug ) {

        $instantiated_tests = $this->get_tests();

        foreach ( $instantiated_tests as $test ) {

            if ( $test->get_slug() === $test_slug ) {
                return true;
            }
        }

        return false;
    }


    function get_test( $test_slug ) {

        if ( $this->test_instantiated( $test_slug ) ) {

            return $this->get_test_from_memory( $test_slug );
        }

        $test = COHO_Feature_Test::get_instance( $test_slug );

        $this->add_test($test);

        return $test;
    }


    /**
     * Check if code should run for current user.
     *
     * @param	string  $test_slug
     * @param	string  $cohort_slug
     * @return	bool
     */
    function code_should_run_for_user( $test_slug, $cohort_slug ) {

        /*
         * Check 1: Test is active?
         */

        $test = $this->get_test( $test_slug );

        if ( !$test->is_active() ) return true;


        /*
         * Check 2: Cohort exists?
         */

        $cohort = $test->get_cohort($cohort_slug);

        if ( $cohort === null ) return true;     // Code runs by default. Code should not run only with explicit and
                                                 // accurate intent.

        /*
         * Check 3: User is in cohort?
         */

        $coho_user = coho_user();

        return $coho_user->user_is_in_cohort( $test->get_id(), $cohort->get_id() );
    }


    /**
     * Return the URL directory to a file within this plugin folder.
     *
     * @param	string  $path   The URL directory to this plugin folder, with trailing '/'
     * @return	string
     */
    function dir_url( $path = '' ) {

        return plugin_dir_url( __FILE__ ) . $path;
    }


    /**
     * Return the path to a file within this plugin folder.
     *
     * @param	string  $path   The relative path from the root of this plugin folder, without trailing '/'
     * @return	string
     */
    function get_path( $path = '' ) {

        return plugin_dir_path( __FILE__ ) . $path;
    }


    /**
     * Require a file, checking if it exists.
     *
     * @param	string  $relative_path      Path relative to plugin source. Without trailing '/'
     */
    function safely_require_once( $relative_path ) {

        $path = $this->get_path( $relative_path );

        if( file_exists($path) ) {

            require_once( $path );
        }
    }


    /**
     * COHORT_ANALYSIS_MAIN Instance.
     *
     * Ensures only one instance of COHORT_ANALYSIS_MAIN is loaded or can be loaded.
     *
     * @static
     *
     * @return COHORT_ANALYSIS_MAIN - Main instance.
     */
    public static function instance() {

        if ( is_null( self::$instance ) ) {

            self::$instance = new self;
        }
        return self::$instance;
    }
}

/**
 * Plugin Main instance.
 *
 * Returns the main instance to prevent the need to use globals.
 *
 * @return COHORT_ANALYSIS_MAIN - Main instance
 */
function coho() {
    return COHORT_ANALYSIS_MAIN::instance();
}

// initialize
coho();

endif;